# «Sedona - Arizona»

 * https://playua20.github.io/Sedona-Arizona-responsive/dist/ - Главная
 * https://playua20.github.io/Sedona-Arizona-responsive/dist/media.html - Медиа
 * https://playua20.github.io/Sedona-Arizona-responsive/dist/send-form.html - feedback форма
 
 > устаревший free домен (2017): 
 
 >* [http://sedona.kl.com.ua/](http://sedona.kl.com.ua/) - Главная
 
 >* [http://sedona.kl.com.ua/media.html](http://sedona.kl.com.ua/media.html) - Медиа
 
 >* [http://sedona.kl.com.ua/send-form.html](http://sedona.kl.com.ua/send-form.html) - рабочая feedback форма




## Особенности:
 * Responsive (mobile, tablet, desktop)
 * Mobile first
 * BEM
 
## Технологии:
 * jQuery
 * Bootstrap-grids
 * Gulp
 * Scss

>*Дизайн шаблона принадлежит ***HtmlAcademy****

@ 2017-2023